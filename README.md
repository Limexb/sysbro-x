<p align="center">
<img width="128" src="https://gitee.com/Limexb/sysbro/raw/master/src/resources/sysbro.svg" >
</p>


<h1 align="center">Sysbro X</h1>


### ✨ 简介
Sysbro X是一款专业的Linux管理助手，可智能清理Linux中的磁盘垃圾，快速释放电脑内存，一键扫描和优化Linux系统，让您的电脑焕然一新！

本项目源于rekols的Sysbro，原作者已经删除了代码，故接手维护。



### 🎨 计划
- [x] 新建文件夹
- [ ] 一个超超超好看的UI
- [ ] 系统概览
- [ ] 空文件夹清理
- [ ] 相似图片清理 感知哈希算法 https://blog.csdn.net/forthcriminson/article/details/8729000
- [ ] 智能清理
- [ ] 优化加速
- [ ] 电池管理
- [ ] 恶意软件清除
- [ ] 整合Oh my dde和yoyo
